#include <stdio.h>

int main()
{
    int number,positive=0,negative=0,zero=0;
    do
    {
        printf("Enter a number \n");
        scanf("%d", &number);
        if (number>0)
        {
            positive++;
        }
        else if (number<0)
        {
            negative++;
        }
        else
        {
            zero++;
        }
    }
    while(number!=-1);
    printf("\nPositive Numbers : %d\nNegative Numbers : %d\nZero Numbers : %d",positive,negative,zero);
return 0;
}