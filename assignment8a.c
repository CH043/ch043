#include <stdio.h>

int main()
{
  int num[5];

  printf("Enter 5 values ");

  for(int i = 0; i < 5; ++i) {
     scanf("%d", &num[i]);
  }

  printf("Displaying the values ");

  for(int i = 0; i < 5; ++i) {
     printf("%d\n",num[i]);
  }
  return 0;
}//write your code here