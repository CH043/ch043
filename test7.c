#include<stdio.h>

struct student
{
    int roll_no,marks;
    char name[25];
};

int main()
{
    int i,j,n;
    struct student stud[100],t;
    printf("Enter the no of students\n");
    scanf("%d",&n);
    
    for(i=0;i<n;i++)
    {
        printf("enter %d student info of as roll_no , name , marks\n",i+1);
        scanf("%d %s %d",&stud[i].roll_no,stud[i].name,&stud[i].marks);
    }
    
    for(i=0;i<n;i++)
    {
        for(j=0;j<n-1;j++)
        {
            if(stud[j].marks<stud[j+1].marks)
            {
                t=stud[j];
                stud[j]=stud[j+1];
                stud[j+1]=t;
            }
        }
    }
    
    printf("\n Student who got the highest marks is\n");
    printf("\n roll_no %d \n name %s \n marks %d \n",stud[0].roll_no,stud[0].name,stud[0].marks);
    

return 0;
}