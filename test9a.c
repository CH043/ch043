#include <stdio.h>
void swap(int * p1, int * p2)
{
int t;
t = *p1;
*p1 = *p2;
*p2 = t;
}
 
int main()
{
int n1,n2;
printf("Enter the numbers to be swapped : ");
scanf("%d%d",&n1,&n2);
printf("Before swapping\n");
printf("Number 1 = %d\n", n1);
printf("Number 2 = %d\n", n2);
 
swap( &n1, &n2);
 
printf("After swapping \n");
printf("Number 1 = %d\n", n1);
printf("Number 2 = %d", n2);
return 0;
}
