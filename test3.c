#include <stdio.h>

int main()
{
   int n, t, s = 0, r;

   printf("Enter an integer\n");
   scanf("%d", &n);

   t = n;

   while (t != 0)
   {
      r = t % 10;
      s = s + r;
      t = t / 10;
   }

   printf("Sum of digits of %d = %d\n", n, s);

   return 0;
}
//write your code here
