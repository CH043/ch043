#include<stdio.h>
float input();
float compute(float);
void output(float);
int main()
{
	float r,area;
	r=input();
	area=compute(r);
	output(area);
	return 0;
}
float input()
{	float r;
	printf("Enter the radius:");
	scanf("%f",&r);
	return r;
}
float compute(float r)
{
	float area;
	area=3.14*r*r;
	return area;
}
void output(float area)
{
	printf("Area is=%f",area);
}

//write your code here