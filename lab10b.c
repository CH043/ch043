#include<stdio.h> 
#include<string.h> 
 
int main()
{       
    
    int len, i= 0, j, flag = 1;
    
    char word[50];
    
    printf("Enter The string: ");
    gets(word);
    j = strlen(word) - 1;
    while(i <= j)
    {
        if (word[i] != word[j])
        {
            flag = 0;
            break;
        }
        i++;  
        j--;
    }
    
    if(flag)
    {
        printf("\n %s is palindrome", word);
    }
    else
    {
        printf("\n %s is not palindrome", word);
    }
    
    return 0;
}